<?php
// Copyright 2011 Toby Zerner, Simon Zerner
// This file is part of esoTalk. Please see the included license file for usage information.

ET::$languageInfo["Polish"] = array(
	"locale" => "pl-PL",
	"name" => "Polski",
	"description" => "Polska paczka językowa. Więcej informacji na <a href='http://tomaszgasior.kao.pl/esotalk' target='_blank'>tomaszgasior.kao.pl/esotalk</a>.",
	"version" => "1.0.0g4 (2015-01-25)",
	"author" => "Tomasz Gąsior",
	"authorEmail" => "kontakt@tomaszgasior.kao.pl",
	"authorURL" => "http://tomaszgasior.kao.pl/esotalk",
	"license" => "GPLv2"
);

// Define the character set that this language uses.
$definitions["charset"] = "utf-8";

$definitions["date.full"] = "%c"; setlocale( LC_ALL, array('pl-PL', 'pl_PL', 'pl_PL.UTF-8', 'plk', 'polish', 'polish_pol', 'polish_poland') );

$definitions["%d day ago"] = "wczoraj";
$definitions["%d days ago"] = "%d dni temu";
$definitions["%d hour ago"] = "1 godzinę temu";
$definitions["%d hours ago"] = "%d godzin(y) temu";
$definitions["%d minute ago"] = "1 minutę temu";
$definitions["%d minutes ago"] = "%d minut(y) temu";
$definitions["%d month ago"] = "1 miesiąc temu";
$definitions["%d months ago"] = "%d miesięcy temu";
$definitions["%d second ago"] = "1 sekundę temu";
$definitions["%d seconds ago"] = "%d sekund(y) temu";
$definitions["%d week ago"] = "tydzień temu";
$definitions["%d weeks ago"] = "%d tygodni(e) temu";
$definitions["%d year ago"] = "rok temu";
$definitions["%d years ago"] = "%d lat(a) temu";

$definitions["%s and %s"] = "%s i %s";
$definitions["%s can view this conversation."] = "%s &ndash; mogą przeglądać ten wątek.";
$definitions["%s changed %s's group to %s."] = "%s zmienia grupę użytkownika %s na %s.";
$definitions["%s changed your group to %s."] = "%s zmienia twoją grupę na %s.";
$definitions["%s conversation"] = "Wątków: %s";
$definitions["%s conversations"] = "Wątków: %s";
$definitions["%s has registered and is awaiting approval."] = "%s zarejestrował(a) się i oczekuje na akcpetację.";
$definitions["%s invited you to %s."] = "%s zaprasza cię do %s.";
$definitions["%s joined the forum."] = "%s rejestruje się.";
$definitions["%s mentioned you in %s."] = "%s cytuje cię w poście %s.";
$definitions["%s post"] = "Postów: %s";
$definitions["%s posted %s"] = "%s &ndash; %s";
$definitions["%s posted in %s."] = "%s pisze w %s.";
$definitions["%s posts"] = "Postów: %s";
$definitions["%s reply"] = "Odpowiedzi: %s";
$definitions["%s replies"] = "Odpowiedzi: %s";
$definitions["%s Settings"] = "Ustawienia %s";
$definitions["%s started the conversation %s."] = "%s rozpoczyna wątek %s.";
$definitions["%s will be able to view this conversation."] = "%s będą mogli przeglądać ten wątek.";
$definitions["%s will be able to:"] = "%s będzie posiadać następujące uprawnienia:";

$definitions["A new version of esoTalk (%s) is available."] = "Nowa wersja esoTalk (%s) jest dostępna.";
$definitions["a private conversation"] = "wątek prywatny";
$definitions["Automatically follow conversations that I reply to"] = "Automatycznie obserwuj wątki, w których odpowiem";
$definitions["Automatically follow private conversations that I'm added to"] = "Automatycznie obserwuj wątki prywatne, do których zostanę dodany";
$definitions["Access the administrator control panel."] = "Dostęp do panelu zarządzania administratora.";
$definitions["Account type"] = "Typ konta";
$definitions["Activate"] = "Aktywuj";
$definitions["Activity"] = "Aktywność";
$definitions["Add"] = "Dodaj";
$definitions["Administration"] = "Administracja";
$definitions["Administrator email"] = "E-mail administratora";
$definitions["Administrator password"] = "Hasło administratora";
$definitions["Administrator username"] = "Nazwa administratora";
$definitions["Advanced Options"] = "Opcje zaawansowane";
$definitions["All Channels"] = "Wszystkie kanały";
$definitions["Allow members to edit their own posts:"] = "Wskaż, jak długo użytkownik mogą edytować własne posty.";
$definitions["Already have an account? <a href='%s' class='link-login'>Log in!</a>"] = "Masz już konto? <a href='%s' class='link-login'>Zaloguj się!</a>";
$definitions["Appearance"] = "Wygląd";
$definitions["Approve"] = "Zaakceptuj";
$definitions["Automatically star conversations that I reply to"] = "Automatycznie obserwuj wątki, w których odpowiem";
$definitions["Avatar"] = "Awatar";

$definitions["Back to channels"] = "Wróć do kanałów";
$definitions["Back to conversation"] = "Wróć do wątku";
$definitions["Back to member"] = "Wróć do użytkownika";
$definitions["Back to members"] = "Wróć do użytkowników";
$definitions["Back to search"] = "Wróć do wyszukiwania";
$definitions["Background color"] = "Kolor tła";
$definitions["Background image"] = "Obraz tła";
$definitions["Base URL"] = "Bazowy URL";
$definitions["Bold"] = "Pogrubienie";
$definitions["By %s"] = "Autor: %s";

$definitions["Can suspend/unsuspend members"] = "Zawieszanie kont użytkowników";
$definitions["Cancel"] = "Anuluj";
$definitions["Change"] = "Zmień";
$definitions["Change %s's Permissions"] = "Zmień uprawnienia %s";
$definitions["Change avatar"] = "Zmień awatar";
$definitions["Change Channel"] = "Zmiana kanału dla wątku";
$definitions["Change channel"] = "Zmień kanał";
$definitions["Change name"] = "Zmień nazwę";
$definitions["Change Password or Email"] = "Zmień hasła lub adresu e-mail";
$definitions["Change Password"] = "Zmiana hasła";
$definitions["Change password"] = "Zmień hasło";
$definitions["Change permissions"] = "Zmień uprawnienia";
$definitions["Channel description"] = "Opis kanału";
$definitions["Channel List"] = "Lista kanałów";
$definitions["Channel title"] = "Tytuł kanału";
$definitions["Channel slug"] = "Nazwa uproszczona";
$definitions["Channels"] = "Kanały";
$definitions["Choose a secure password of at least %s characters"] = "Wybierz hasło o długości co najmniej %s znaków.";
$definitions["Choose what people will see when they first visit your forum."] = "Wybierz, co użytkownicy będą widzieć po wejściu na forum.";
$definitions["Click on a member's name to remove them."] = "Kliknij nazwę użytkownika, aby go usunąć.";
$definitions["Close registration"] = "Zamknięta rejestracja";
$definitions["Confirm password"] = "Potwierdź hasło";
$definitions["Controls"] = "Zarządzaj";
$definitions["Conversation"] = "Wątek";
$definitions["Conversations participated in"] = "Wątki, w których uczestniczy";
$definitions["Conversations started"] = "Rozpoczęte wątki";
$definitions["Conversations"] = "Wątki";
$definitions["Copy permissions from"] = "Kopiuj uprawnienia z kanału: ";
$definitions["Create Channel"] = "Utwórz kanał";
$definitions["Create Group"] = "Utwórz grupę";
$definitions["Create Member"] = "Utwórz użytkownika";
$definitions["Customize how users can become members of your forum."] = "Ustal, jak użytkownicy mogą dołączać do twojego forum.";
$definitions["Customize your forum's appearance"] = "Dopasuj wygląd forum";

$definitions["Dashboard"] = "Kokpit";
$definitions["Default forum language"] = "Domyślny język forum";
$definitions["<strong>Delete</strong> all conversations forever."] = "<strong>Usuń</strong> wszystkie wątki na zawsze.";
$definitions["Delete Channel"] = "Usuwanie kanału";
$definitions["Delete conversation"] = "Usuń wątek";
$definitions["Delete member"] = "Usuń użytkownika";
$definitions["Delete Member"] = "Usuwanie użytkownika";
$definitions["<strong>Delete this member's posts.</strong> All of this member's posts will be marked as deleted, but will be able to be restored manually."] = "<strong>Usuń posty tego użytkownika.</strong> Wszystkie posty tego użytkownika zostaną oznaczone jako usunięte, ale będzie można je przywrócić ręcznie.";
$definitions["Delete"] = "Usuń";
$definitions["Deleted %s by %s"] = "Usunięto %s przez %s";
$definitions["Deny"] = "Odmów";  // ========== ?? =====================
$definitions["Disable"] = "Wyłącz";
$definitions["Discard"] = "Odrzuć";
$definitions["Don't allow other users to see when I am online"] = "Nie pozwól innym użytkownikom widzieć, że jestem dostępny";
$definitions["Don't have an account? <a href='%s' class='link-join'>Sign up!</a>"] = "Nie masz konta? <a href='%s' class='link-join'>Zarejestruj się!</a>";
$definitions["Don't repeat"] = "Nie powtarzaj";
$definitions["Don't require users to confirm their account"] = "Nie wymagaj od użytkowników potwierdzania ich kont";

$definitions["Edit Channel"] = "Edycja kanału";
$definitions["Edit Group"] = "Edycja grupy";
$definitions["Edit member groups"] = "Edytuj grupy użytkowników";
$definitions["Edit your profile"] = "Edytuj swój profil";
$definitions["Edit"] = "Edytuj";
$definitions["Edited %s by %s"] = "Edytowano %s przez %s";
$definitions["Editing permissions"] = "Uprawnienia edycji";
$definitions["Email me when I'm added to a private conversation"] = "Wyślij mi e-mail, gdy zostanę dodany do wątku prywatnego";
$definitions["Email me when someone mentions me in a post"] = "Wyślij mi e-mail, gdy ktoś zacytuje mnie w poście";
// -----> definitions.ChannelNotifications.php
$definitions["Email me when someone posts in a conversation I have followed"] = "Wyślij mi e-mail, gdy ktoś napisze w wątku, który obserwuję";
// -----> definitions.MemberNotifications.php
$definitions["Email"] = "E-mail";
$definitions["Enable"] = "Włącz";
$definitions["Enabled"] = "Włączone";
$definitions["Enter a conversation title"] = "Wprowadź tytuł wątku";
$definitions["Error"] = "Błąd";
$definitions["esoTalk version"] = "Wersja esoTalk";
$definitions["Everyone"] = "Wszyscy";

$definitions["Fatal Error"] = "O nie! Błąd krytyczny!";
$definitions["Feed"] = "Kanał";
$definitions["Filter by name or group..."] = "Filtruj wg nazwy lub grupy&hellip;";
$definitions["Find this post"] = "Znajdź ten post";
$definitions["First posted"] = "Pierwszy post";
$definitions["Follow"] = "Obserwuj";
// -----> definitions.ChannelNotifications.php
$definitions["Following"] = "Obserwuję";
$definitions["For %s seconds"] = "Przez %s sekund";
$definitions["Forever"] = "Zawsze";
$definitions["Forgot?"] = "Zapomniane?";
$definitions["Forgot Password"] = "Przypominanie hasła";
$definitions["Forum"] = "Forum";
$definitions["Forum header"] = "Nagłówek forum";
$definitions["Forum language"] = "Język forum";
$definitions["Forum privacy"] = "Prywatność forum";
$definitions["Forum Settings"] = "Ustawienia";
$definitions["Forum Statistics"] = "Statystyki forum";
$definitions["Forum title"] = "Tytuł forum";
$definitions["forumDescription"] = "%s, %s, %s";

$definitions["Give this group the 'moderate' permission on all existing channels"] = "Uprawnienia do moderacji we wszystkich istniejących kanałach";
$definitions["Global permissions"] = "Uprawnienia globalne";
$definitions["Go to top"] = "Skocz do góry";
$definitions["Group name"] = "Nazwa grupy";
$definitions["group.administrator"] = "Administrator";
$definitions["group.administrator.plural"] = "Administratorzy";
$definitions["group.guest"] = "Gość";
$definitions["group.guest.plural"] = "Goście";
$definitions["group.member"] = "Użytkownik";
$definitions["group.member.plural"] = "Użytkownicy";
$definitions["group.Moderator"] = "Moderator";
$definitions["group.Moderator.plural"] = "Moderatorzy";
$definitions["group.suspended"] = "Zawieszony";
$definitions["Groups can be used to categorize members and give them certain privileges."] = "Użyj grup do podziału użytkowników i do nadawania im przywilejów.";
$definitions["Groups"] = "Grupy";
$definitions["Guests can view the:"] = "Wskaż, co mogą przeglądać goście (niezalogowani)";

$definitions["Header"] = "Nagłówek";
$definitions["Header color"] = "Kolor nagłówka";
$definitions["Heads Up!"] = "W górę!";  // =======================================
$definitions["Hide"] = "Ukryj";
$definitions["Home page"] = "Strona główna";
$definitions["HTML is allowed."] = "HTML jest dozwolony.";

$definitions["If you run into any other problems or just want some help with the installation, feel free to ask for assistance at the <a href='%s'>esoTalk support forum</a>."] = "W razie problemów z instalacją skorzystaj z pomocy <a href='%s'>forum esoTalk (w jęz. angielskim)</a>.";
$definitions["Install esoTalk"] = "Instaluj esoTalk";
$definitions["Install My Forum"] = "Instaluj forum";
$definitions["Installed Languages"] = "Zainstalowane języki";
$definitions["Installed Plugins"] = "Zainstalowane wtyczki";
$definitions["Installed plugins"] = "Zainstalowane wtyczki";
$definitions["Installed Skins"] = "Zainstalowane skórki";
$definitions["Installed skins"] = "Zainstalowane skórki";
$definitions["is %s"] = "&ndash; %s";

$definitions["Joined"] = "Rejestracja";
$definitions["just now"] = "teraz";

$definitions["Keep me logged in"] = "Zapamiętaj mnie";
$definitions["<strong>Keep this member's posts.</strong> All of this member's posts will remain intact, but will show [deleted] as the author."] = "<strong>Zachowaj posty tego użytkownika.</strong> Wszystkie posty tego użytkownika pozostaną nienaruszone, ale nie będzie pokazany ich autor.";

$definitions["label.draft"] = "Szkic";
$definitions["label.locked"] = "Zablokowany";
$definitions["label.ignored"] = "Ignorowany";
$definitions["label.private"] = "Prywatny";
$definitions["label.sticky"] = "Przypięty";
$definitions["Labels"] = "Etykiety";
$definitions["Last active"] = "Aktywność";
$definitions["Last active %s"] = "Ostatnia aktywność: %s";
$definitions["Latest"] = "ostatni post";
$definitions["Latest News"] = "Ostatnie wiadomości";
$definitions["Loading..."] = "Ładowanie&hellip;";
$definitions["Lock"] = "Zablokuj";
$definitions["Log In"] = "Zaloguj się";
$definitions["Log Out"] = "Wyloguj się";

$definitions["Make member and online list visible to:"] = "Listę użytkowników i osób dostępnych ustaw widoczną dla:";
$definitions["Manage Channels"] = "Zarządzanie kanałami";
$definitions["Manage Groups"] = "Zarządzanie grupami";
$definitions["Manage Languages"] = "Zarządzanie językami";
$definitions["Manage your forum's channels (categories)"] = "Skonfiguruj kanały (działy) forum";
$definitions["Mark as read"] = "Oznacz jako przeczytany";
$definitions["Mark as unread"] = "Oznacz jako nieprzeczytany";
$definitions["Mark all as read"] = "Oznacz wszystko jako przeczytane";
$definitions["Mark listed as read"] = "Oznacz poniższe jako przeczytane";
$definitions["Maximum size of %s. %s."] = "Maksymalna wielkość: %s. Dozwolone formaty: %s.";
$definitions["Member groups"] = "Grupy użytkowników";
$definitions["Member list"] = "Lista użytkowników";
$definitions["Member List"] = "Lista użytkowników";
$definitions["Member privacy"] = "Prywatność użytkowników";
$definitions["Members"] = "Użytkownicy";
$definitions["Members Allowed to View this Conversation"] = "Użytkownicy uprawnieni do przeglądania tego wątku";
$definitions["Members Awaiting Approval"] = "Użytkownicy oczekujący na akceptację";
$definitions["Members Online"] = "Dostępni użytkownicy";
$definitions["Members"] = "Użytkownicy";
$definitions["Mobile skin"] = "Skórka mobilna";
$definitions["Moderate"] = "Moderacja";
$definitions["<strong>Move</strong> conversations to the following channel:"] = "<strong>Przenieś</strong> wątki do tego kanału:";
$definitions["Mute conversation"] = "Schowaj wątek";
$definitions["MySQL Database"] = "Baza danych MySQL";
$definitions["MySQL Host"] = "Adres hosta MySQL";
$definitions["MySQL Password"] = "Hasło MySQL";
$definitions["MySQL queries"] = "Zapytania MySQL";
$definitions["MySQL Table Prefix"] = "Prefiks tabeli MySQL";
$definitions["MySQL Username"] = "Nazwa użytkownika MySQL";
$definitions["MySQL version"] = "Wersja MySQL";

$definitions["Name"] = "Nazwa";
$definitions["never"] = "nigdy";
$definitions["%s new"] = "%s new";
$definitions["New conversation"] = "Nowy wątek";
$definitions["New Conversation"] = "Nowy wątek";
$definitions["New conversations in the past week"] = "Nowe wątki w ostatnim tygodniu";
$definitions["New email"] = "Nowy e-mail";
$definitions["New members in the past week"] = "Nowi użytkownicy w ostatnim tygodniu";
$definitions["New password"] = "Nowe hasło";
$definitions["New posts in the past week"] = "Nowe posty w ostatnim tygodniu";
$definitions["New username"] = "Nowa nazwa użytkownika";
$definitions["Next Step"] = "Następny krok";
$definitions["Next"] = "Dalej";
$definitions["No preview"] = "Brak podglądu";
$definitions["No"] = "Nie";
$definitions["Notifications"] = "Powiadomienia";
$definitions["Now"] = "Teraz";

$definitions["OK"] = "OK";
$definitions["Online"] = "Dostępny";
$definitions["online"] = "dostępny";
$definitions["Only allow members of this group to see who else is in it"] = "Tylko członkom tej grupy pozwól widzieć, kto jeszcze do niej należy";
$definitions["Open registration"] = "Otwarta rejestracja";
$definitions["optional"] = "opcjonalnie";
$definitions["Order By:"] = "Porządkuj wg:";
$definitions["Original Post"] = "pierwszy post";

$definitions["Page Not Found"] = "Strona nie została znaleziona";
$definitions["Password"] = "Hasło";
$definitions["PHP version"] = "Wersja PHP";
$definitions["Plugins"] = "Wtyczki";
$definitions["Post a Reply"] = "Odpowiedz";
$definitions["Post count"] = "Licznik postów";
$definitions["Posts"] = "Posty";
$definitions["Powered by"] = "Wspierane przez";
$definitions["Preview"] = "Podgląd";
$definitions["Previous"] = "Wstecz";

$definitions["Quote"] = "Cytuj";
$definitions["quote"] = "cytat";

$definitions["Read more"] = "Przeczytaj więcej";
$definitions["Recent posts"] = "Ostatnie posty";
$definitions["Recover Password"] = "Odzyskaj hasło";
$definitions["Registered members"] = "Zarejestrowani użytkownicy";
$definitions["Registration"] = "Rejestracja";
$definitions["Registration Closed"] = "Zamknięta rejestracja";
$definitions["Remove avatar"] = "Usuń awatar";
$definitions["Rename Member"] = "Zmiana nazwy użytkownika";
$definitions["Reply"] = "Odpowiadanie";
// -----> definitions.ReportBug.php
$definitions["Require administrator approval"] = "Wymagaj zaakceptowania przez administratora";
$definitions["Require users to confirm their email address"] = "Wymagaj od użytkowników potwierdzania ich adresu e-mail";
$definitions["Restore"] = "Przywróć";
$definitions["restore"] = "przywróć";
$definitions["Reset"] = "Wyczyść";

$definitions["Save Changes"] = "Zapisz zmiany";
$definitions["Save Draft"] = "Zapisz szkic";
$definitions["Search conversations..."] = "Przeszukaj wątki&hellip;";
$definitions["Search within this conversation..."] = "Przeszukaj ten wątek&hellip;";
$definitions["Search"] = "Szukaj";
$definitions["See the private conversations I've had with %s"] = "Zobacz moje wątki prywatne z %s";
$definitions["Set a New Password"] = "Ustawianie nowego hasła";
$definitions["Settings"] = "Ustawienia";
$definitions["Show an image in the header"] = "Pokaż obraz w nagłówku";
$definitions["Show in context"] = "Pokaż w kontekście"; //=================
$definitions["Show matching posts"] = "Pokaż odpowiadające posty"; ////////////////////
$definitions["Show the channel list by default"] = "Lista kanałów";
$definitions["Show the conversation list by default"] = "Lista wątków";
$definitions["Show the forum title in the header"] = "Pokaż tytuł forum w nagłówku";
$definitions["Sign Up"] = "Zarejestruj się";
$definitions["Skins"] = "Skórki";
$definitions["Sort By"] = "Sortuj wg";
$definitions["Specify Setup Information"] = "Informacje instalacji";
$definitions["Star to receive notifications"] = "Obserwuj, aby otrzymywać powiadomienia";
$definitions["Starred"] = "Obserwuję";
$definitions["Start"] = "Rozpoczynanie";
$definitions["Start a conversation"] = "Rozpocznij wątek";
$definitions["Start a new conversation"] = "Rozpocznij nowy wątek";
$definitions["Start a private conversation with %s"] = "Rozpocznij wątek prywatny z %s";
$definitions["Start Conversation"] = "Rozpocznij wątek";
$definitions["Starting a conversation"] = "Rozpoczyna wątek";
$definitions["Statistics"] = "Statystyki";
$definitions["statistic.conversation.plural"] = "wątków: %s";
$definitions["statistic.conversation"] = "wątków: %s";
$definitions["statistic.member.plural"] = "użytkowników: %s";
$definitions["statistic.member"] = "użytkowników: %s";
$definitions["statistic.online.plural"] = "dostępnych: %s";
$definitions["statistic.online"] = "dostępnych: %s";
$definitions["statistic.post.plural"] = "postów: %s";
$definitions["statistic.post"] = "postów: %s";
$definitions["Sticky"] = "Przypnij";
$definitions["Subscribe"] = "Subskrybuj";
$definitions["Subscribed"] = "Subskrybowany";
$definitions["Subscription"] = "Subskrybcja";
$definitions["Success!"] = "Udało się!";
$definitions["Suspend member"] = "Zawieś użytkownika";
$definitions["Suspend members."] = "Zawieszanie konta użytkowników.";
$definitions["Suspend"] = "Zawieś";

$definitions["To get started with your forum, you might like to:"] = "Aby zacząć korzystać z forum:";

$definitions["Unapproved"] = "Niezaakceptowany";
$definitions["Unhide"] = "Pokaż";
$definitions["Uninstall"] = "Odinstaluj";
$definitions["Unlock"] = "Odblokuj";
$definitions["Unmute conversation"] = "Odkryj wątek";
$definitions["Unstarred"] = "Nieobserwowany";
$definitions["Unsticky"] = "Odepnij";
$definitions["Unsubscribe new users by default"] = "Domyślnie niesubskrybowany przez nowych użytkowników";
$definitions["Unsubscribe"] = "Nie subksrybuj";
$definitions["Unsubscribed"] = "Niesubskrybowany";
$definitions["Unsuspend member"] = "Odwołaj zawieszenie użytkownika";
$definitions["Unsuspend"] = "Odwołaj zawieszenie";
$definitions["Until someone replies"] = "Dopóki nikt nie odpowie";
$definitions["Untitled conversation"] = "Wątek bez tytułu";
$definitions["Use a background image"] = "Użyj obrazu tła";
$definitions["Upgrade esoTalk"] = "Aktualizuj esoTalk";
$definitions["Use for mobile"] = "Użyj dla telefonów";
$definitions["Use friendly URLs"] = "Użyj przyjaznych adresów URL";
$definitions["Used to verify your account and subscribe to conversations"] = "Używany do weryfikacji konta i subskrypcji wątków.";
$definitions["Username"] = "Nazwa użytkownika";
$definitions["Username or Email"] = "Nazwa użytkownika lub e-mail";

$definitions["View %s's profile"] = "Zobacz profil %s";
$definitions["View all notifications"] = "Zobacz wszystkie powiadomienia";
$definitions["View more"] = "Zobacz więcej";
$definitions["View your profile"] = "Zobacz swój profil";
$definitions["View"] = "Czytanie";
$definitions["Viewing: %s"] = "teraz przegląda: %s";
$definitions["viewingPosts"] = "<b>%s-%s</b> z %s postów";

$definitions["Warning"] = "Ostrzeżenie";
$definitions["Welcome to esoTalk!"] = "Witaj w esoTalk!";
$definitions["We've logged you in and taken you straight to your forum's administration panel. You're welcome."] = "Zalogowałeś się do swojego panelu administracyjnego. Witaj.";
$definitions["Write a reply..."] = "Napisz odpowiedź&hellip;";

$definitions["Yes"] = "Tak";
$definitions["You can manage channel-specific permissions on the channels page."] = "Możesz też zmienić uprawnienia indywidualnie dla kanału.";
$definitions["Your current password"] = "Obecne hasło";


// Messages.
$definitions["message.404"] = "O, nie! Nie znaleziono strony, której poszukujesz. Spróbuj się cofnąć lub kliknąć którykolwiek odnośnik. Albo zrób cokolwiek innego&hellip;";
$definitions["message.accountNotYetApproved"] = "Administrator nie zaakceptował jeszcze twojego konta. Pamiętaj, cierpliwość jest cnotą!";
$definitions["message.ajaxDisconnected"] = "Nie można połączyć się z serwerem. Poczekaj kilka sekund i <a href='javascript:jQuery.ETAjax.resumeAfterDisconnection()'>spróbuj ponownie</a> lub <a href='' onclick='window.location.reload();return false'>odśwież stronę</a>.";
$definitions["message.ajaxRequestPending"] = "Hej, wciąż trwa przetwarzanie danych! Jeśli opuścisz teraz tę stronę, utracisz wszystkie wprowadzone zmiany, więc poczekaj jeszcze chwilkę, dobrze?";
$definitions["message.avatarError"] = "Wystąpił problem z wysyłaniem awatara. Sprawdź, czy wysyłasz plik w akceptowanym formacie (.jpg, .png, lub .gif) oraz czy nie jest zbyt duży.";
$definitions["message.cannotDeleteLastChannel"] = "Chcesz usunąć ostatni kanał! W takim razie gdzie umieścisz wątki? Zastanów się nad tym.";
$definitions["message.cannotEditSinceReply"] = "Nie możesz już edytować tego postu, ponieważ ktoś już na niego odpowiedział.";
$definitions["message.changesSaved"] = "Zmiany zostały zapisane.";
$definitions["message.channelsHelp"] = "Kanały służą do podziału wątków na twoim forum. Możesz stworzyć ich tyle, ile zechcesz i zmieniać ich kolejność, przeciągając je.";
$definitions["message.channelSlugTaken"] = "Ta nazwa uproszczona jest już używana w innym kanale.";
$definitions["message.confirmDelete"] = "Czy na pewno usunąć? Nie będzie można tego cofnąć.";
$definitions["message.confirmDiscardPost"] = "Odpowiedź nie została zapisana jako szkic. Czy na pewno chcesz ją odrzucić?";
$definitions["message.confirmEmail"] = "Zanim zaczniesz używać nowo utworzonego konta, musisz potwierdzić swój adres e-mail. W&nbsp;ciągu kilku minut otrzymasz wiadomość, w której znajdzie się odnośnik aktywujący konto.";
$definitions["message.confirmLeave"] = "Poczekaj chwilkę! Jeśli opuścisz tę stronę, wszystkie wprowadzone zmiany zostaną utracone. Czy na pewno chcesz to zrobić?";
$definitions["message.connectionError"] = "esoTalk nie może połączyć się z serwerem MySQL. Zwrócony błąd:<br/>%s";
$definitions["message.conversationDeleted"] = "Wątek został usunięty.";
$definitions["message.conversationNotFound"] = "Z jakiegoś powodu wątek nie może zostać wyświetlony. Być może nie istnieje albo nie masz uprawnień do przeglądania go.";
$definitions["message.cookieAuthenticationTheft"] = "Z przyczyn związanych z bezpieczeństwem nie można zalogować cię automatycznie. Zaloguj się ręcznie.";
$definitions["message.deleteChannelHelp"] = "Jeżeli usuniesz ten kanał, nie będzie można tego cofnąć! No, chyba że zbudujesz wehikuł czasu, ale to nie takie proste&hellip; Wszystkie wątki z tego kanalu możesz przenieść do innego.";
$definitions["message.emailConfirmed"] = "Twoje konto zostało aktywowane i możesz już brać udział w wątkach. A może warto <a href='".URL("conversation/start")."'>stworzyć własny</a>?";
$definitions["message.emailDoesntExist"] = "Ten e-mail nie istnieje w bazie danych. Może jakaś literówka?";
$definitions["message.emailNotYetConfirmed"] = "Musisz potwierdzić e-mail zanim się zalogujesz! Jeśli nie otrzymujesz wiadomości potwierdzającej, <a href='%s'>kliknij tutaj, aby została wysłana ponownie</a>.";
$definitions["message.emailTaken"] = "Już istnieje użytkownik z tym e-mailem.";
$definitions["message.empty"] = "Musisz wypełnić to pole.";
$definitions["message.emptyPost"] = "Wypadałoby coś wpisać w treści postu&hellip;";
$definitions["message.emptyTitle"] = "Tytuł wątku nie może być pusty. Jak ktoś później kliknąłby na taki pusty tytuł? Przemyśl to.";
$definitions["message.esoTalkAlreadyInstalled"] = "<strong>esoTalk jest zainstalowany.</strong><br/><small>Aby go reinstalować, musisz usunąć plik <code>config/config.php</code>.</small>";
$definitions["message.esoTalkUpdateAvailable"] = "Nowa wersja esoTalk, %s, jest teraz dostępna.";
$definitions["message.esoTalkUpdateAvailableHelp"] = "Zaleca się mieć zawsze najnowszą wersję esoTalk ze względów bezpieczeństwa. Poza tym to też nowe, ekscytujące funkcjonalności!";
$definitions["message.esoTalkUpToDate"] = "Ta wersja esoTalk jest aktualna.";
$definitions["message.esoTalkUpToDateHelp"] = "Autor esoTalk jest biednym studentem, który spędził nad nim mnóstwo godzin pracy. Jeżeli esoTalk spodobało ci się i chcesz odwdzięczyć się autorowi, zastanów się nad <a href='%s' target='_blank'>dotacją</a>.";
$definitions["message.fatalError"] = "esoTalk napotkało błąd krytyczny uniemożliwiający prawidłowe działanie. Spróbuj ponownie lub <a href='%1\$s' target='_blank'>poproś o pomoc na forum</a>.";
$definitions["message.fileUploadFailed"] = "Coś poszło nie tak i wybrany plik nie może być przesłany. Być może jest w złym formacie lub zbyt duży?";
$definitions["message.fileUploadFailedMove"] = "Wysłany plik nie może zostać skopiowany do właściwego miejsca. Skontaktuj się z administratorem forum.";
$definitions["message.fileUploadNotImage"] = "Wysłany plik nie jest obrazem w jednym z akceptowanych formatów.";
$definitions["message.fileUploadTooBig"] = "Wybrany plik nie może być wysłany, ponieważ jest za duży.";
$definitions["message.forgotPasswordHelp"] = "Każdemu zdarza się czasem zapomnieć hasło, nie przejmuj się. Wystarczy wpisać adres e-mail, a zostaną tam wysłane dalsze instrukcje.";
$definitions["message.fulltextKeywordWarning"] = "Usuń z poszukiwanego wyrażenia krótkie, kilkuliterowe słowa oraz spójniki (np. <em>lub</em>, <em>oraz</em>, <em>w</em>).";
$definitions["message.gambitsHelp"] = "Gambity to frazy, które opisują to, czego szukasz. Kliknij na frazę, aby umieścić ją w polu wyszukiwania. Kliknij dwukrotnie, by szukać natychmiat. Standardowe wyszukiwanie oczywiście też działa!";
$definitions["message.gdNotEnabledWarning"] = "<strong>Rozszerzenie GD nie jest aktywne.</strong> Jest wymagane do zapisu i zmiany rozmiarów awatarów. Skontaktuj się z administratorem.";
$definitions["message.greaterMySQLVersionRequired"] = "<strong>Na serwerze musi być zainstalowane MySQL w wersji 4 lub wyższej oraz <a href='http://php.net/manual/en/mysql.installation.php' target='_blank'>dodatek MySQL włączony w PHP</a>.</strong> Dokonaj instalacji lub aktualizacji, aby spełniać wymagania lub skontaktuj się z administratorem.";
$definitions["message.greaterPHPVersionRequired"] = "<strong>Na serwerze musi być zainstalowane PHP 5.0.0 lub nowsze, aby uruchomić esoTalk.</strong> Zaktualizuj PHP lub skontaktuj się z administratorem serwera.";
$definitions["message.incorrectLogin"] = "Dane logowania się niepoprawne.";
$definitions["message.incorrectPassword"] = "Twoje obecne hasło jest niepoprawne.";
$definitions["message.installerAdminHelp"] = "esoTalk użyje poniższych informacji do ustawienia konta administratora tego forum.";
$definitions["message.installerFilesNotWritable"] = "<strong>esoTalk nie może zapisać tych plików lub folderów: %s.</strong> Aby to naprawić, musisz przejść do tych plików, używając klienta FTP, i ustawić <code>chmod</code> na wartość <code>0777</code>.";
$definitions["message.installerWelcome"] = "Skonfiguruj minimalistyczne forum esoTalk, wypełniając poniższy formularz. <br />W razie kłopotów poproś o pomoc na <a href='%s' target='_blank'>forum esoTalk (w jęz. angielskim)</a>.";
$definitions["message.invalidChannel"] = "Wybrano niewłaściwy kanał.";
$definitions["message.invalidEmail"] = "Ten e-mail wydaje się być nieprawidłowy&hellip;";
$definitions["message.invalidUsername"] = "Musisz wybrać nazwę użytkownika, która ma między 3 a 20 znakami alfanumerycznymi.";
$definitions["message.javascriptRequired"] = "Ta strona wymaga do właściwego funkcjonowania JavaScript. Włącz go.";
$definitions["message.languageUninstalled"] = "Język został odinstalowany.";
$definitions["message.locked"] = "Ten wątek jest <strong>zablokowany</strong>, więc nie możesz w nim odpowiedzieć.";
$definitions["message.loginToParticipate"] = "Zaloguj się, aby tworzyć wątki i odpowiadać na posty.";
$definitions["message.logInToReply"] = "<a href='%1\$s' class='link-login'>Zaloguj się</a> lub <a href='%2\$s' class='link-join'>zarejestruj się</a>, aby odpowiadać.";
$definitions["message.logInToSeeAllConversations"] = "<a href='".URL("user/login")."' class='link-login'>Zaloguj się</a>, aby zobaczyć kanały i wątki ukryte dla gości.";
$definitions["message.memberNotFound"] = "Nie ma użytkownika o takiej nazwie.";
$definitions["message.memberNoPermissionView"] = "Ten użytkownik nie może zostać dodany, ponieważ nie ma uprawnień do przeglądania kanału, w którym znajduje się ten wątek.";
$definitions["message.nameTaken"] = "Wpisana nazwa jest już zarejestrowana.";
$definitions["message.newSearchResults"] = "Pojawiły się nowe wyniki wyszukiwania. <a href='%s'>Odśwież</a>";
$definitions["message.noActivity"] = "%s nic na tym forum nie robi!";
$definitions["message.noChannels"] = "Brak widocznych kanałów.";  /// -================= ???????? ======
$definitions["message.noMembersOnline"] = "Żaden użytkownik nie jest w tej chwili zalogowany.";
$definitions["message.noNotifications"] = "Brak powiadomień.";
$definitions["message.noPermission"] = "Nie masz uprawnień do wykonania tej czynności.";
$definitions["message.noPermissionToReplyInChannel"] = "Nie masz uprawnień do odpowiadania w wątkach w tym kanale.";
$definitions["message.noPluginsInstalled"] = "Żadne wtyczki nie są obecnie zainstalowane.";
$definitions["message.noSearchResults"] = "Nie zostały znalezione wątki pasujące do twojego wyszukiwania.";
$definitions["message.noSearchResultsMembers"] = "Nie zostali znalezieni użytkownicy pasujący do twojego wyszukiwania.";
$definitions["message.noSearchResultsPosts"] = "Nie zostały znalezione posty pasujące do twojego wyszukiwania.";
$definitions["message.noSkinsInstalled"] = "Żadne skórki nie są obecnie zainstalowane.";
$definitions["message.notWritable"] = "Plik <code>%s</code> nie jest zapisywalny. Spróbuj zmienić <code>chmod</code> na <code>777</code> lub jeśli plik nie istnieje, ustaw <code>chmod</code> dla folderu, w którym powinien się znaleźć.";
$definitions["message.pageNotFound"] = "Poszukiwana strona nie została odnaleziona.";
$definitions["message.passwordChanged"] = "W porządku, twoje hasło zostało zmienione. Możesz się już zalogować. Postaraj się jednak już więcej tego hasła nie zapomnieć, dobrze?";
$definitions["message.passwordEmailSent"] = "OK, wysłano już do ciebie wiadomość z odnośnikiem do resetowania hasła. Jeśli za kilka chwil nie otrzymasz e-maila, sprawdź folder ze spamem.";
$definitions["message.passwordsDontMatch"] = "Hasła nie są zgodne.";
$definitions["message.passwordTooShort"] = "Hasło jest za krótkie.";
$definitions["message.pluginCannotBeEnabled"] = "Wtyczka <em>%s</em> nie może zostać włączona: %s";
$definitions["message.pluginDependencyNotMet"] = "Aby włączyć tę wtyczkę, musisz mieć zainstalowane i właczone %s w wersji %s.";
$definitions["message.pluginUninstalled"] = "Wtyczka została odinstalowana.";
$definitions["message.postNotFound"] = "Post, którego szukasz nie został znaleziony.";
$definitions["message.postTooLong"] = "Twój post jest zbyt długi. Maksymalna liczba znaków to %s.";
$definitions["message.preInstallErrors"] = "Poniższe błędy musisz naprawić przed kontynuowaniem instalacji.";
$definitions["message.preInstallWarnings"] = "Zostały odnalezione poniższe błędy. Możesz kontynuować instalację, nie naprawiając ich, jednak funkcjonalność esoTalk może przez to być ograniczona.";
$definitions["message.reduceNumberOfGambits"] = "Zmniejsz liczbę fraz gambit oraz szukanych wyrażeń, by znaleźć więcej wątków.";
$definitions["message.registerGlobalsWarning"] = "<strong>Ustawienie register_globals w PHP jest włączone.</strong> esoTalk może w tej sytuacji działać, zaleca się jednak wyłączenie tego ustawienia w celu zwiększenia bezpieczeństwa i zapobieganiu problemom.";
$definitions["message.registrationClosed"] = "Rejestracja na tym forum jest zamknięta.";
$definitions["message.removeDirectoryWarning"] = "Nie usunięto katalogu <code>%s</code>, co należy zrobić, aby nie ułatwiać zadania hakerom.";
$definitions["message.safeModeWarning"] = "<strong>Tryb bezpieczny jest włączony.</strong> Może on powodować problemy z działaniem esoTalk, ale możesz kontynuować, jeśli nie masz możliwości wyłączenia go.";
$definitions["message.searchAllConversations"] = "Spróbuj przeszukać wszystkie wątki.";
$definitions["message.setNewPassword"] = "Jakie chcesz ustawić nowe hasło?";
$definitions["message.skinUninstalled"] = "Skórka została odinstalowana.";
$definitions["message.suspended"] = "Moderator <strong>zawiesił</strong> twoje konto. Do czasu odblokowania twojego konta wiele na tym forum nie zdziałasz.";
$definitions["message.suspendMemberHelp"] = "Zawieszenie użytkownika %s uniemożliwi mu rozpoczynanie i odpowiadanie w&nbsp;wątkach oraz przeglądanie wątków prywatnych. Będzie więc posiadać te same uprawnienia, co gość.";
$definitions["message.tablePrefixConflict"] = "Instalator wykrył inną instalację esoTalk w tej samej bazie danych i z tym samym prefiksem.<br/>• Aby nadpisać tę instalację, kliknij ponownie przycisk &bdquo;Instaluj &rdquo;. <strong>Wszystkie dane zostaną utracone.</strong><br/>• Jeśli chcesz stworzyć inną instalację poza istniejącą, <strong>zmień prefiks tabeli</strong>.";
$definitions["message.unsuspendMemberHelp"] = "Odwołanie zawieszenia użytkownika %s umożliwi mu ponowne uczestnictwo w&nbsp;dyskusjach na forum.";
$definitions["message.upgradeSuccessful"] = "esoTalk został pomyślnie zaktualizowany.";
$definitions["message.waitForApproval"] = "Zanim zaczniesz korzystać z nowo utworzonego konta, administrator forum musi je zaakceptować.";
$definitions["message.waitToReply"] = "Musisz odczekać %s sekund. Weź głęboki oddech i spróbuj ponownie.";
$definitions["message.waitToSearch"] = "Zwolnij! Próbujesz wykonać zbyt wiele wyszukań. Poczekaj %s sekund i spróbuj ponownie.";


// Emails.
$definitions["email.header"] = "<p>Cześć, %s!</p>";
$definitions["email.footer"] = "<p>(Jeśli nie chcesz otrzymywać takich wiadomości ponownie, możesz <a href='%s'>zmienić ustawienia powiadomień</a>.)</p>";

$definitions["email.confirmEmail.subject"] = "Potwierdzenie adresu e-mail";
$definitions["email.confirmEmail.body"] = "<p>Ktoś, używając tego adresu e-mail, zarejestrował się na forum „%1\$s”.</p><p>Jeśli to ty, kliknij ten odnośnik, a twoje konto zostanie aktywowane:<br>%2\$s</p>";

$definitions["email.approved.subject"] = "Zaakceptowano twoje konto";
$definitions["email.approved.body"] = "<p>Twoje konto na forum „%1\$s” zostało zaakceptowane.</p><p>Kliknij poniższy odnośnik, aby się zalogować i brać udział w dyskusjach:<br>%2\$s</p>";

$definitions["email.forgotPassword.subject"] = "Resetowanie hasla";
$definitions["email.forgotPassword.body"] = "<p>Ktoś zgłosił żądanie resetowania hasła do twojego konta na forum „%1\$s”. Jeśli nie chcesz zmieniać hasła, zignoruj tę wiadomość, a nic się nie stanie.</p><p>Jeśli jednak zapomniałeś(-aś) hasła i chcesz je zmienić, kliknij poniższy odnośnik:<br>%2\$s</p>";

$definitions["email.mention.subject"] = "[Cytat] %2\$s";   // [Mentioned by %1\$s] %2\$s
$definitions["email.mention.body"] = "<p><strong>%1\$s</strong> zacytował(-a) cię w poście w wątku <strong>„%2\$s”</strong>.</p><hr>%3\$s<hr><p>Aby zobaczyć post w kontekście, kliknij poniższy odnośnik:<br>%4\$s</p>";

$definitions["email.privateAdd.subject"] = "[Wątek prywatny] %1\$s";
$definitions["email.privateAdd.body"] = "<p>Dodano cię do wątku prywatnego zatytułowanego <strong>„%1\$s”</strong>.</p><hr>%2\$s<hr><p>Aby zobaczyć ten wątek, kliknij poniższy odnośnik:<br>%3\$s</p>";

$definitions["email.post.subject"] = "[Nowa odpowiedź] %1\$s";
$definitions["email.post.body"] = "<p><strong>%1\$s</strong> odpowiedział(-a) w wątku, który obserwujesz: <strong>„%2\$s”</strong>.</p><hr>%3\$s<hr><p>Aby go zobaczyć, kliknij poniższy odnośnik:<br>%4\$s</p>";


// Translating the gambit system can be quite complex, but we'll do our best to get you through it. :)
// Note: Don't use any html entities in these definitions, except for: &lt; &gt; &amp; &#39;

// Simple gambits
// These gambits are pretty much evaluated as-they-are.
// tag:, author:, contributor:, and quoted: are combined with a value after the colon (:).
// For example: tag:video games, author:myself
$definitions["gambit.author:"] = "autor:";
$definitions["gambit.contributor:"] = "uczestnik:";
$definitions["gambit.member"] = "użytkownik";
$definitions["gambit.myself"] = "ja";
$definitions["gambit.draft"] = "szkice";
$definitions["gambit.locked"] = "zablokowane";
$definitions["gambit.order by newest"] = "sortuj wg świeżości";
$definitions["gambit.order by replies"] = "sortuj wg odpowiedzi";
$definitions["gambit.private"] = "prywatne";
$definitions["gambit.random"] = "losowo";
$definitions["gambit.reverse"] = "odwrotnie";
$definitions["gambit.starred"] = "obserwowane";
$definitions["gambit.ignored"] = "ignorowane";
$definitions["gambit.sticky"] = "przypięte";
$definitions["gambit.unread"] = "nieprzeczytane";
$definitions["gambit.limit:"] = "limit:";

// Aliases
// These are gambits which tell the gambit system to use another gambit.
// In other words, when you type "active today", the gambit system interprets it as if you typed "active 1 day".
// The first of each pair, the alias, can be anything you want.
// The second, however, must fit with the regular expression pattern defined below (more on that later.)
$definitions["gambit.active today"] = "aktywne dziś"; // what appears in the gambit cloud
$definitions["gambit.active 1 day"] = "aktywne 1 dzień"; // what it actually evaluates to

$definitions["gambit.has replies"] = "ma odpowiedzi";
$definitions["gambit.has >0 replies"] = "ma >0 odpowiedzi";
$definitions["gambit.has >10 replies"] = "ma >10 odpowiedzi";

$definitions["gambit.has no replies"] = "nie ma odpowiedzi";
$definitions["gambit.has 0 replies"] = "ma 0 odpowiedzi";

$definitions["gambit.dead"] = "zapomniane";
$definitions["gambit.active >30 day"] = "aktywne >30 dni";

// Units of time
// These are used in the active gambit.
// ex. "[active] [>|<|>=|<=|last] 180 [second|minute|hour|day|week|month|year]"
$definitions["gambit.second"] = "sekund";
$definitions["gambit.minute"] = "minut";
$definitions["gambit.hour"] = "godzin";
$definitions["gambit.day"] = "dni";
$definitions["gambit.week"] = "tygodni";
$definitions["gambit.month"] = "miesięcy";
$definitions["gambit.year"] = "lat";
$definitions["gambit.last"] = "ostatnie"; // as in "active last 180 days"
$definitions["gambit.active"] = "aktywne"; // as in "active last 180 days"

// Now the hard bit. This is a regular expression to test for the "active" gambit.
// The group (?<a> ... ) is the comparison operator (>, <, >=, <=, or last).
// The group (?<b> ... ) is the number (ex. 24).
// The group (?<c> ... ) is the unit of time.
// The languages of "last" and the units of time are defined above.
// However, if you need to reorder the groups, do so carefully, and make sure spaces are written as " *".
$definitions["gambit.gambitActive"] = "/^{$definitions["gambit.active"]} *(?<a>>|<|>=|<=|{$definitions["gambit.last"]})? *(?<b>\d+) *(?<c>{$definitions["gambit.second"]}|{$definitions["gambit.minute"]}|{$definitions["gambit.hour"]}|{$definitions["gambit.day"]}|{$definitions["gambit.week"]}|{$definitions["gambit.month"]}|{$definitions["gambit.year"]})/";

// These appear in the tag cloud. They must fit the regular expression pattern where the ? is a number.
// If the regular expression pattern has been reordered, these gambits must also be reordered (as well as the ones in aliases.)
$definitions["gambit.active last ? hours"] = "{$definitions["gambit.active"]} {$definitions["gambit.last"]} ? {$definitions["gambit.hour"]}";
$definitions["gambit.active last ? days"] = "{$definitions["gambit.active"]} {$definitions["gambit.last"]} ? {$definitions["gambit.day"]}";

// This is similar to the regular expression for the active gambit, but for the "has n reply(s)" gambit.
// Usually you just need to change the "has" and "repl".
$definitions["gambit.gambitHasNReplies"] = "/^ma *(?<a>>|<|>=|<=)? *(?<b>\d+) *odpowied/";





// ========================================================================================================

$definitions["Choose a Channel"] = "Wybór kanału dla wątku";
$definitions["Primary color"] = "Kolor podstawowy";
$definitions["Change username"] = "Zmień nazwę użytkownika";

$definitions["Ignore conversation"] = "Ignoruj wątek";
$definitions["Unignore conversation"] = "Nie ignoruj wątku";

$definitions["%s Members"] = "Lista użytkowników (%s)";
$definitions["Privacy"] = "Prywatność";
$definitions["Version %s"] = "Wersja: %s";

$definitions["Welcome to esoTalk"] = "Witaj w esoTalk";
$definitions["Admin Username"] = "Nazwa administratora";
$definitions["Admin Password"] = "Hasło administratora";
$definitions["Confirm Password"] = "Potwierdź hasło";
$definitions["Admin Email"] = "E-mail administratora";

$definitions["Members who are part of this group can be listed by searching for the group name in the member list."] = "Członkowie tej grupy będą mogli być wyszukiwani według nazwy grupy na liście użytkowniów."; /////////////////////////


