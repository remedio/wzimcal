<?php
// Copyright 2011 Toby Zerner, Simon Zerner
// This file is part of esoTalk. Please see the included license file for usage information.


$definitions["message.trackingIdHelp"] = "Aby uzyskać identyfikator śledzenia, przejdź do sekcji <em>Administracja</em> w&nbsp;usłudze Google Analytics i wybierz pozycję <em>Ustawienia usługi</em>.";

$definitions["Tracking ID"] = "Identyfikator śledzenia";
