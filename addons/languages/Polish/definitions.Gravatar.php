<?php
// Copyright 2011 Toby Zerner, Simon Zerner
// This file is part of esoTalk. Please see the included license file for usage information.


$definitions["Change your avatar on <a href='http://gravatar.com' target='_blank'>Gravatar.com</a>."] = "Ustaw swój awatar na stronie <a href='http://gravatar.com' target='_blank'>Gravatar.com</a>.";

$definitions["Default imageset"] = "Domyślny zestaw obrazów";
$definitions["esoTalk default"] = "domyślny w esoTalk";
$definitions["Default"] = "Domyślny";