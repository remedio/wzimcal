<?php
// Copyright 2011 Toby Zerner, Simon Zerner
// This file is part of esoTalk. Please see the included license file for usage information.


$definitions["Server"]  = "Serwer";
$definitions["Username"]  = "Nazwa użytkownika";
$definitions["Password"]  = "Hasło";
$definitions["Port"]  = "Port";
$definitions["Authentication"]  = "Autoryzacja";
$definitions["Normal"]  = "Normalna";