<?php
// Copyright 2011 Toby Zerner, Simon Zerner
// This file is part of esoTalk. Please see the included license file for usage information.


$definitions["Like"] = "Lubię to";
$definitions["Unlike"] = "Już tego nie lubię";

$definitions["%s likes this."] = "%s lubi to.";
$definitions["%s like this."] = "%s lubią to.";
$definitions["%s and %s"] = "%s i %s";
$definitions["%s others"] = "%s innych";

$definitions["Members Who Liked This Post"] = "Użytkownicy, którzy lubią ten post";