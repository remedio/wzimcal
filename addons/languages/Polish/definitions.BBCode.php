<?php
// Copyright 2011 Toby Zerner, Simon Zerner
// This file is part of esoTalk. Please see the included license file for usage information.


$definitions["Code"] = "Kod";
$definitions["Image"] = "Obraz";
$definitions["Link"] = "Odnośnik";
$definitions["Strike"] = "Przekreślenie";
$definitions["Header"] = "Nagłówek";
$definitions["Italic"] = "Pochylenie";
$definitions["Bold"] = "Pogrubienie";