<?php
// Copyright 2011 Toby Zerner, Simon Zerner
// This file is part of esoTalk. Please see the included license file for usage information.


$definitions["Profiles"] = "Profile";
$definitions["Manage Profile Fields"] = "Zarządzanie polami profili";
$definitions["About"] = "O mnie";

$definitions["Create Field"] = "Utwórz pole";
$definitions["Edit Field"] = "Edytuj pole";

$definitions["Field name"] = "Nazwa pola";
$definitions["Field description"] = "Opis pola";
$definitions["Input type"] = "Rodzaj pola";
$definitions["Options"] = "Opcje";

$definitions["Enter one option per line"] = "Wprowadź jedną opcję w jednej linii.";
$definitions["Show field on posts"] = "Pokaż zawartość pola w postach";
$definitions["Hide field from guests"] = "Ukryj pole przed gośćmi";
$definitions["Searchable in member list"] = "Wyszukiwalne na liście użytkowników";

$definitions["Text"] = "Pole tekstowe";
$definitions["Textarea"] = "Pole wieloliniowe";
$definitions["Select"] = "Pole wyboru";
$definitions["Radio buttons"] = "Przyciski opcji";
$definitions["Checkboxes"] = "Przyciski zaznaczenia (checkboksy)";
$definitions["Member reference"] = "Odnośnik do innego użytkownika";

