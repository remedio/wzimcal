<?php
// Copyright 2011 Toby Zerner, Simon Zerner
// This file is part of esoTalk. Please see the included license file for usage information.


$definitions["message.attachmentNotFound"] = "Załącznik nie może zostać wyświetlony. Nie istnieje lub nie masz uprawnień do przeglądania go.";

$definitions["Allowed file types"] = "Dozwolone typy plików";
$definitions["Enter file extensions separated by a space. Leave blank to allow all file types."] = "Wprowadź rozszerzenia plików rozdzielone spacją. Pozostaw puste, aby zezwolić na wszystkie typy plików.";
$definitions["Max file size"] = "Maks. rozmiar pliku";
$definitions["In bytes. Leave blank for no limit."] = "W bajtach. Pozostaw puste, aby limitu nie było.";

$definitions["Embed in post"] = "Wstaw ten załącznik do postu";

$definitions["Attachments"] = "Załączniki";
$definitions["Attach a file"] = "Dodaj załącznik";
$definitions["Drop files to upload"] = "Przeciągnij tutaj pliki, aby załączyć je do postu.";