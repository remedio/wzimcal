<?php
// Copyright 2011 Toby Zerner, Simon Zerner
// This file is part of esoTalk. Please see the included license file for usage information.


$definitions["Word filters"]  = "Filtrowanie słów";
$definitions["message.wordFilterInstructions"]  = "Wpisz każde słowo w nowej linii. Opcjonalnie możesz wpisać zamiennik filtrowanego określenia - za znakiem |. Jeśli nie ustalisz zamiennika, słowo zostanie zastąpione gwiazdkami (*). Wielkość liter nie ma znaczenia. Wyrażenia regularne są dozwolone.";