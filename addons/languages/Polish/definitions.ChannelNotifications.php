<?php
// Copyright 2011 Toby Zerner, Simon Zerner
// This file is part of esoTalk. Please see the included license file for usage information.


$definitions["Email me when someone posts in a channel I have followed"] = "Wyślij mi e-mail, gdy ktoś napisze w kanale, który obserwuję";

$definitions["Follow to receive notifications"] = "Obserwuj, aby otrzymywać powiadomienia";

$definitions["email.postChannel.subject"] = "[Nowy post] %2\$s";
$definitions["email.postChannel.body"] = "<p><strong>%1\$s</strong> napisał(-a) w wątku znajdującym się w kanale, który obserwujesz: <strong>„%2\$s”</strong>.</p><hr>%3\$s<hr><p>Aby go zobaczyć, kliknij poniższy odnośnik:<br>%4\$s</p>";