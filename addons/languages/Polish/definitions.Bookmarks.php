<?php
// Copyright 2011 Toby Zerner, Simon Zerner
// This file is part of esoTalk. Please see the included license file for usage information.


$definitions["gambit.bookmarked"] = "zakładki";

$definitions["Bookmark"] = "Dodaj do zakładek";
$definitions["Unbookmark"] = "Usuń z zakładek";
