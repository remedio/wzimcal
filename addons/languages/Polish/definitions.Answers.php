<?php
// Copyright 2011 Toby Zerner, Simon Zerner
// This file is part of esoTalk. Please see the included license file for usage information.


$definitions["Answered by %s"]  = "%s odpowiedział(a) na to pytanie";
$definitions["See post in context"]  = "Zobacz post w kontekście";
$definitions["Remove answer"]  = "Usuń odpowiedź";

$definitions["This post answered my question"]  = "Ten post odpowiedział na moje pytanie";
$definitions["Answer"]  = "Odpowiedź";

$definitions["label.answered"]  = "Rozwiązany";