<?php
// Copyright 2011 Toby Zerner, Simon Zerner
// This file is part of esoTalk. Please see the included license file for usage information.


$definitions["MySQL queries"] = "Zapytania MySQL";
$definitions["Page loaded in %s seconds"] = "Strona załadowana w %s sekund";
$definitions["POST + GET + FILES information"] = "Informacje POST + GET + FILES";
$definitions["SESSION + COOKIE information"] = "Informacje SESSION + COOKIE";

$definitions["Database"] = "Baza danych";
$definitions["Upgrade Database"] = "Zaktualizuj bazę danych";