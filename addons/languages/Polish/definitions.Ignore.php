<?php
// Copyright 2011 Toby Zerner, Simon Zerner
// This file is part of esoTalk. Please see the included license file for usage information.


$definitions["Ignore member"] = "Ignoruj tego użytkownika";
$definitions["Unignore member"] = "Nie ignoruj tego użytkownika";

$definitions["Post from %s hidden."] = "Posty użytkownika %s są ukrywane.";
$definitions["Show"] = "Pokaż";