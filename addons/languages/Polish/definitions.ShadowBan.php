<?php
// Copyright 2011 Toby Zerner, Simon Zerner
// This file is part of esoTalk. Please see the included license file for usage information.


$definitions["Shadow ban"] = "Zbanuj niewidocznie";
$definitions["Un-shadow ban"] = "Cofnij ban niewidoczny";

$definitions["Shadow Delete"] = "Usuń niewidocznie dla użytkownika";
$definitions["Un-shadow Delete"] = "Cofnij usunięcie niewidocznie dla użytkownika";

$definitions["<span>This member has been shadow-banned</span>"] = "<span>Ten użytkownik jest niewidocznie zbanowany &mdash; nie jest o tym informowany</span>";
$definitions["This member has been shadow-banned"] = "Ten użytkownik jest niewidocznie zbanowany &mdash; nie jest o tym informowany";