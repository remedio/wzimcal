<?php
// Copyright 2011 Toby Zerner, Simon Zerner
// This file is part of esoTalk. Please see the included license file for usage information.


$definitions["Email me when there is a new post by a member I have followed"] = "Wyślij mi e-mail, gdy pojawią się nowe posty użytkownika, którego obserwuję";

$definitions["email.postMember.subject"] = "[Nowy post] %1\$s";
$definitions["email.postMember.body"] = "<p><strong>%1\$s</strong> napisał(-a) w wątku: <strong>„%2\$s”</strong>.</p><hr>%3\$s<hr><p>Aby zobaczyć nowy post, kliknij poniższy odnośnik:<br>%4\$s</p>";